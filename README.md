# Authors

* Ahmed Magdy Mohamed  <a.magdy@a1works.com>

* Ahmed Gaber Hassanien  <eng.ahmedgaber@gmail.com>


#  Design Highlights

* Single Threaded Matcher per product (security)

* Transaction handling inside Matcher thread

* Optimistic Concurrency Control using undoable commands in case of race conditions.

* Matcher thread pool size is defined according to the number of available processors


#  Architecture

![architecture](https://bytebucket.org/ahmed_and_ahmed/contest2016/raw/master/resources/images/architecture.png "Architecture")



#  Work Flow

![work_flow](https://bytebucket.org/ahmed_and_ahmed/contest2016/raw/master/resources/images/work-flow.png "Work Flow")


#  Assumptions

* We assume that the ConnectionFactory provided by GFT InitialContext should be used without any tuning from our side.

* We assume that the number of MQ thread pool size must be proportional to the Matchers thread pool in order to guarantee fairness.


#  Future Enhancements

If the system is required to work continuously for longer periods of time, some changes need to be done.

* Disruptor Ring buffers to read transactions from matcher threads continuously in a lock free fashion.

* TTL based data structures to delete old committed commands that are 60 seconds old or more, so that they are guaranteed that they will not be undone.



