package com.gft.digitalbank.exchange.solution.services.matcher.commands;

/**
 * Created by magdy on 18.08.16.
 */
public enum CommandStatus {
    NEW,
    DOING,
    DONE,
    UNDOING,
    UNDONE;
}
