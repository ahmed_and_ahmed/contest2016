package com.gft.digitalbank.exchange.solution;

import com.gft.digitalbank.exchange.Exchange;
import com.gft.digitalbank.exchange.listener.ProcessingListener;
import com.gft.digitalbank.exchange.solution.models.Context;
import com.gft.digitalbank.exchange.solution.services.MatchingService;
import com.gft.digitalbank.exchange.solution.services.MessageConsumptionService;
import com.gft.digitalbank.exchange.solution.utils.DefaultExecutorServiceFactory;
import com.gft.digitalbank.exchange.solution.utils.ExecutorName;
import com.gft.digitalbank.exchange.solution.utils.WithLogger;
import com.gft.digitalbank.exchange.verification.env.Config;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Your solution must implement the {@link Exchange} interface.
 */
public class StockExchange implements Exchange {

    private Context context;
    private MessageConsumptionService consumptionService;
    private MatchingService matchingService;

    public StockExchange() {
        this.context = new Context();
    }

    @Override
    public void register(ProcessingListener processingListener) {
        context.setProcessingListener(processingListener);
    }

    @Override
    public void setDestinations(List<String> list) {
        context.setDestinations(list);
        this.matchingService = new MatchingService(context);
        this.consumptionService = new MessageConsumptionService(context, matchingService);
    }

    @Override
    public void start() {
        consumptionService.start();
    }

}
