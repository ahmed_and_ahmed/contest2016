package com.gft.digitalbank.exchange.solution.services;

import com.gft.digitalbank.exchange.solution.models.Context;
import com.gft.digitalbank.exchange.solution.utils.ExecutorName;
import com.gft.digitalbank.exchange.solution.utils.WithLogger;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public final class MessageConsumptionService implements WithLogger {

    private final MessageProcessor messageProcessor;
    private final Map<String, Consumer> consumers;
    private final CountDownLatch countdownLatch;
    private final Context context;

    public MessageConsumptionService(
            Context context,
            MessageProcessor messageProcessor
    ) {
        this.context = context;
        List<String> destinations = context.getDestinations();
        this.countdownLatch = new CountDownLatch(destinations.size());
        this.messageProcessor = messageProcessor;
        this.consumers = new ConcurrentHashMap<>();
        for (String destination : destinations) {
            consumers.computeIfAbsent(
                    destination,
                    (dest) -> new OrderConsumer(context, dest, countdownLatch, messageProcessor)
            );
        }
    }


    public void start() {
        CompletableFuture.runAsync(() -> {
            consumers.values().forEach(Consumer::startReceivingMessages);
            try {
                countdownLatch.await();
            } catch (InterruptedException e) {
                getLogger().error(e.getMessage(), e);
                throw new RuntimeException(e);
            } finally {
                messageProcessor.allMessagesProcessed();
            }
        }, context.getExecutorFactory().getInstance(ExecutorName.Execution));
    }


}
