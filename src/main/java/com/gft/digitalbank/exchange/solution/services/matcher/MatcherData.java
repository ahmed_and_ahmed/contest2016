package com.gft.digitalbank.exchange.solution.services.matcher;

import com.gft.digitalbank.exchange.model.OrderBook;
import com.gft.digitalbank.exchange.model.OrderEntry;
import com.gft.digitalbank.exchange.model.Transaction;
import com.gft.digitalbank.exchange.model.orders.Side;
import com.gft.digitalbank.exchange.solution.models.Order;
import com.gft.digitalbank.exchange.solution.utils.WithLogger;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.annotation.concurrent.NotThreadSafe;

@NotThreadSafe
public final class MatcherData implements OrderBookAndTransactionsProvider, WithLogger {

    private final String product;
    private final PriorityQueue<Order.BuyOrder> buyWorkBook;
    private final PriorityQueue<Order.SellOrder> sellWorkBook;
    private final List<Transaction> transactions;

    public MatcherData(String product){
        this.product = product;
        this.buyWorkBook = new PriorityQueue<>();
        this.sellWorkBook = new PriorityQueue<>();
        this.transactions = new LinkedList<>();
    }

    public String getProduct() {
        return product;
    }

    public PriorityQueue<Order.BuyOrder> getBuyWorkBook() {
        return buyWorkBook;
    }


    public PriorityQueue<Order.SellOrder> getSellWorkBook() {
        return sellWorkBook;
    }

    public Optional<? extends Order> findOrder(String broker, int orderId) {
        Optional<Order.BuyOrder> optOrder = buyWorkBook.stream().filter((buyOrder -> (buyOrder.getId() == orderId && buyOrder.getBroker().equals(broker)))).findFirst();
        if (!optOrder.isPresent()) {
            return sellWorkBook.stream().filter((sellOrder -> (sellOrder.getId() == orderId && sellOrder.getBroker().equals(broker)))).findFirst();
        } else {
            return optOrder;
        }
    }

    public void removeOrder(Order order) {
        if (order.getSide() == Side.BUY) {
            buyWorkBook.remove(order);
        } else {
            sellWorkBook.remove(order);
        }
    }

    public void addOrder(Order order) {
        if (!order.getProduct().equals(product)) {
            String errorMsg = String.format("Cannot add an order of product(%s) to the WorkBook(%s)", order.getProduct(), product);
            getLogger().error(errorMsg);
            throw new IllegalArgumentException(errorMsg);
        }
        if (order instanceof Order.BuyOrder) {
            buyWorkBook.add((Order.BuyOrder) order);
        } else if (order instanceof Order.SellOrder) {
            sellWorkBook.add((Order.SellOrder) order);
        }
    }

    @Override
    public Optional<OrderBook> getOrderBook() {
        // not thread safe but should not cause problems in our case @see getTransactions()
        List<OrderEntry> buyOrders = getOrderEntries(buyWorkBook);
        List<OrderEntry> sellOrders = getOrderEntries(sellWorkBook);
        if (buyOrders.isEmpty() && sellOrders.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(new OrderBook(getProduct(), buyOrders, sellOrders));
        }
    }

    @Override
    public Collection<Transaction> getTransactions() {
        // This could be unsafe if we return this object as an
        // OrderBookAndTransactionsProvider to another thread while the matcher data is being
        // updated at the same time. However, according to the business logic we only get the
        // transactions and order books after all matching is done so we know that the matcher
        // data will not be updated concurrently.
        return transactions;
    }





    private List<OrderEntry> getOrderEntries(PriorityQueue<? extends Order> workBookQueue) {
        AtomicInteger autoId = new AtomicInteger(0);
        return workBookQueue
                .stream()
                .sorted()
                .map((order) -> mapOrderToOrderEntry(order, autoId.incrementAndGet()))
                .collect(Collectors.toList());
    }

    private static OrderEntry mapOrderToOrderEntry(Order order, int id) {
        return new OrderEntry(id, order.getBroker(), order.getAmount(), order.getPrice(), order.getClient());
    }

    public int nextTransactionId() {
        return transactions.size()+1;
    }
}
