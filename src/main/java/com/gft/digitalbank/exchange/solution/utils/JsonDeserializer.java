package com.gft.digitalbank.exchange.solution.utils;

import java.lang.reflect.Type;

public interface JsonDeserializer {
    <T> T fromJson(String json, Type typeOfT);
}
