package com.gft.digitalbank.exchange.solution.models;

import com.gft.digitalbank.exchange.model.orders.BrokerMessage;
import com.gft.digitalbank.exchange.model.orders.CancellationOrder;
import com.gft.digitalbank.exchange.model.orders.ModificationOrder;
import com.gft.digitalbank.exchange.model.orders.PositionOrder;
import com.gft.digitalbank.exchange.model.orders.ShutdownNotification;

public enum MessageTypeAndClass {
    ORDER(PositionOrder.class),
    MODIFICATION(ModificationOrder.class),
    CANCEL(CancellationOrder.class),
    SHUTDOWN_NOTIFICATION(ShutdownNotification.class);

    private Class<? extends BrokerMessage> messageClass;

    MessageTypeAndClass(Class<? extends BrokerMessage> messageClass) {
        this.messageClass = messageClass;
    }


    public Class<? extends BrokerMessage> getMessageClass() {
        return messageClass;
    }
}
