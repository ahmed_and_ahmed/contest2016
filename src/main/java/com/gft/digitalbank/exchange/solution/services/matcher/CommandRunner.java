package com.gft.digitalbank.exchange.solution.services.matcher;

import com.gft.digitalbank.exchange.model.orders.BrokerMessage;
import com.gft.digitalbank.exchange.solution.services.matcher.commands.Command;
import com.gft.digitalbank.exchange.solution.utils.WithLogger;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

final class CommandRunner implements WithLogger {

    private final Deque<Command> commandsStack;

    public CommandRunner(){
        this.commandsStack = new LinkedList<>();
    }

    public boolean shouldUndoCommands(long timestamp){
        if (commandsStack.isEmpty()) {
            return false;
        }
        return commandsStack.peek().getTimestamp() > timestamp;
    }

    public void doCommand(Command command){
        command.doCommand();
        commandsStack.push(command);
    }

    public List<BrokerMessage> undoToTimestampAndReturnOriginalMessages(long timestamp) {
        List<BrokerMessage> messages = new LinkedList<>();
        Command command;
        while ((command = commandsStack.peek()) != null && command.getTimestamp() > timestamp) {
            commandsStack.pop();
            command.undoCommand().ifPresent(messages::add);
        }
        return messages;
    }

}
