package com.gft.digitalbank.exchange.solution.services.matcher.commands;

import com.gft.digitalbank.exchange.model.orders.BrokerMessage;
import com.gft.digitalbank.exchange.solution.services.matcher.MatcherData;

import java.util.Optional;

public interface Command {

    String getProduct();

    MatcherData getMatcherData();

    long getTimestamp();

    void doCommand();

    Optional<BrokerMessage> undoCommand();

    CommandStatus getStatus();
}
