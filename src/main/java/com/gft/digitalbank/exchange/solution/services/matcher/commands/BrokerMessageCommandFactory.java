package com.gft.digitalbank.exchange.solution.services.matcher.commands;

import com.gft.digitalbank.exchange.model.orders.BrokerMessage;
import com.gft.digitalbank.exchange.model.orders.CancellationOrder;
import com.gft.digitalbank.exchange.model.orders.ModificationOrder;
import com.gft.digitalbank.exchange.model.orders.PositionOrder;
import com.gft.digitalbank.exchange.solution.services.matcher.MatcherData;
import com.gft.digitalbank.exchange.solution.utils.WithLogger;

public final class BrokerMessageCommandFactory implements WithLogger {

    private MatcherData matcherData;

    public BrokerMessageCommandFactory(MatcherData matcherData){
        this.matcherData = matcherData;
    }

    public BrokerMessageCommand getCommand(BrokerMessage message){
        if (message instanceof PositionOrder) {
            return new PositionOrderCommand(matcherData, (PositionOrder) message);
        } else if (message instanceof ModificationOrder) {
            return new ModifyOrderCommand(matcherData, (ModificationOrder) message);
        } else if (message instanceof CancellationOrder) {
            return new CancelOrderCommand(matcherData, (CancellationOrder) message);
        } else {
            IllegalArgumentException ex = new IllegalArgumentException(
                    String.format("Cannot create a command with message of type : %s",
                            message.getClass().getSimpleName()));
            getLogger().error(ex.getMessage(), ex);
            throw ex;
        }
    }
}
