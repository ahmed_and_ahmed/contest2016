package com.gft.digitalbank.exchange.solution.models;

import com.gft.digitalbank.exchange.model.orders.MessageType;
import com.gft.digitalbank.exchange.model.orders.PositionOrder;
import com.gft.digitalbank.exchange.model.orders.Side;
import com.gft.digitalbank.exchange.solution.utils.WithLogger;

import java.util.concurrent.atomic.AtomicInteger;


public abstract class Order implements Comparable<Order>, WithLogger {

    private PositionOrder wrappedImmutableOrder;
    private AtomicInteger amount;

    public static Order createInstance(PositionOrder positionOrder) {
        Side side = positionOrder.getSide();
        if (side.equals(Side.SELL)) {
            return new SellOrder(positionOrder);
        } else {
            return new BuyOrder(positionOrder);
        }
    }

    public static String buildUniqueId(String brokerName, int orderId) {
        return String.format("%s-%d", brokerName, orderId);
    }


    protected Order(PositionOrder immutableOrder) {
        wrappedImmutableOrder = immutableOrder;
        amount = new AtomicInteger(immutableOrder.getDetails().getAmount());
    }

    public MessageType getMessageType() {
        return wrappedImmutableOrder.getMessageType();
    }

    public long getTimestamp() {
        return wrappedImmutableOrder.getTimestamp();
    }

    public int getId() {
        return wrappedImmutableOrder.getId();
    }

    public String getBroker() {
        return wrappedImmutableOrder.getBroker();
    }

    public String getClient() {
        return wrappedImmutableOrder.getClient();
    }

    public String getProduct() {
        return wrappedImmutableOrder.getProduct();
    }

    public Side getSide() {
        return wrappedImmutableOrder.getSide();
    }

    public int getPrice() {
        return wrappedImmutableOrder.getDetails().getPrice();
    }

    public int decreaseAndGetAmount(int decrementValue) {
        return amount.getAndAdd(-decrementValue);
    }

    public int increaseAndGetAmount(int incrementValue) {
        return amount.getAndAdd(incrementValue);
    }

    public int getAmount(){
        return amount.get();
    }

    @Override
    public int compareTo(Order o) {
        if (o == null) return 1;
        if (!this.getSide().equals(o.getSide())) {
            String msg = String.format("Cannot compare a %s to a %s order", this.getSide(), o.getSide());
            getLogger().error(msg);
            throw new IllegalArgumentException(msg);
        }
        int diff = (
                getSide().equals(Side.BUY) ?
                Integer.compare(o.getPrice(), this.getPrice()) :
                Integer.compare(this.getPrice(), o.getPrice())
        );
        if (diff == 0) {
            diff = Long.compare(this.getTimestamp(), o.getTimestamp());
            if (diff == 0) {
                diff = this.getBroker().compareTo(o.getBroker());
                if (diff == 0) {
                    diff = Integer.compare(this.getId(), o.getId());
                }
            }
        }
        return diff;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Order) {
            return wrappedImmutableOrder.equals(((Order) obj).getPositionOrder());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return wrappedImmutableOrder.hashCode();
    }

    @Override
    public String toString(){
        return wrappedImmutableOrder.toString();
    }

    public PositionOrder getPositionOrder() {
        return wrappedImmutableOrder;
    }



    public static final class SellOrder extends Order {

        private SellOrder(PositionOrder positionOrder) {
            super(positionOrder);
        }
    }

    public static final class BuyOrder extends Order {

        private BuyOrder(PositionOrder positionOrder) {
            super(positionOrder);
        }

    }

}
