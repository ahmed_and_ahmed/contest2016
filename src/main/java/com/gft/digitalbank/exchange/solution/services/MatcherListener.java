package com.gft.digitalbank.exchange.solution.services;

import com.gft.digitalbank.exchange.solution.services.matcher.Matcher;

public interface MatcherListener {

    void matcherClosed(Matcher matcher);
}
