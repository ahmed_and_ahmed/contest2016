package com.gft.digitalbank.exchange.solution.utils;

import java.util.concurrent.ExecutorService;

public interface ExecutorServiceFactory {
    ExecutorService getInstance(ExecutorName name);
}
