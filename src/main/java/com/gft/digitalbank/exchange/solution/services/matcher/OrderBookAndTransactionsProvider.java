package com.gft.digitalbank.exchange.solution.services.matcher;

import com.gft.digitalbank.exchange.model.OrderBook;
import com.gft.digitalbank.exchange.model.Transaction;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

public interface OrderBookAndTransactionsProvider {
    Optional<OrderBook> getOrderBook();
    Collection<Transaction> getTransactions();
}
