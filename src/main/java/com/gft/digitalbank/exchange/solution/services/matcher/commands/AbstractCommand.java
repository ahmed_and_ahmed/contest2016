package com.gft.digitalbank.exchange.solution.services.matcher.commands;

import com.gft.digitalbank.exchange.model.orders.BrokerMessage;
import com.gft.digitalbank.exchange.solution.services.matcher.MatcherData;
import com.gft.digitalbank.exchange.solution.utils.WithLogger;

import java.util.Optional;
import java.util.concurrent.Callable;

public abstract class AbstractCommand implements Command, WithLogger {

    private final String product;
    private final MatcherData matcherData;
    private long timestamp;
    private CommandStatus status;

    protected AbstractCommand(MatcherData matcherData, long timestamp){
        status = CommandStatus.NEW;
        this.product = matcherData.getProduct();
        this.timestamp = timestamp;
        this.matcherData = matcherData;
    }

    @Override
    public String getProduct() {
        return product;
    }

    @Override
    public MatcherData getMatcherData() {
        return matcherData;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public void doCommand() {
        status = CommandStatus.DOING;
        try {
            this.timestamp = getDoAction().call();
        } catch (Exception e) {
            getLogger().error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
        status = CommandStatus.DONE;
    }

    @Override
    public Optional<BrokerMessage> undoCommand() {
        Optional<BrokerMessage> message;
        status = CommandStatus.UNDOING;
        try {
            message = getUndoAction().call();
        } catch (Exception e) {
            getLogger().error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
        status = CommandStatus.UNDONE;
        return message;
    }

    @Override
    public CommandStatus getStatus() {
        return status;
    }



    /**
     * returns a Callable<Long> that when called returns the timestamp of the command
     */
    abstract protected Callable<Long> getDoAction();

    abstract protected Callable<Optional<BrokerMessage>> getUndoAction();
}
