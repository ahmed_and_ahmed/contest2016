package com.gft.digitalbank.exchange.solution.services;

import com.gft.digitalbank.exchange.model.orders.BrokerMessage;
import com.gft.digitalbank.exchange.solution.utils.WithLogger;

public interface MessageProcessor extends WithLogger {
    void allMessagesProcessed();
    void processMessage(BrokerMessage message);
}
