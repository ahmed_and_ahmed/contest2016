package com.gft.digitalbank.exchange.solution.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;

public interface WithLogger {

    default Logger getLogger() {
        return LoggerFactory.getLogger(this.getClass());
    }
}