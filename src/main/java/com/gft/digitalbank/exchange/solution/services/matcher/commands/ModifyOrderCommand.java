package com.gft.digitalbank.exchange.solution.services.matcher.commands;

import com.gft.digitalbank.exchange.model.orders.BrokerMessage;
import com.gft.digitalbank.exchange.model.orders.ModificationOrder;
import com.gft.digitalbank.exchange.solution.models.ModifiedPositionOrder;
import com.gft.digitalbank.exchange.solution.models.Order;
import com.gft.digitalbank.exchange.solution.services.matcher.MatcherData;

import java.util.Optional;
import java.util.concurrent.Callable;

public final class ModifyOrderCommand extends BrokerMessageCommand {

    private ModificationOrder modificationOrder;
    private Order orderBeforeModification;
    private Order modifiedOrder;    // used for undo only

    public ModifyOrderCommand(MatcherData matcherData, ModificationOrder modificationOrder){
        super(matcherData, modificationOrder);
        this.modificationOrder = modificationOrder;
    }

    @Override
    public String toString(){
        return String.format("ModifyOrderCommand( %s )", this.modificationOrder.toString());
    }



    @Override
    protected Callable<Long> getDoAction() {
        return () -> {
            getMatcherData().findOrder(modificationOrder.getBroker(), modificationOrder.getModifiedOrderId()).ifPresent((order) -> {
                this.orderBeforeModification = order;
                getMatcherData().removeOrder(order);
                this.modifiedOrder = Order.createInstance(new ModifiedPositionOrder(order.getPositionOrder(), modificationOrder));
                getMatcherData().addOrder(this.modifiedOrder);
            });
            return modificationOrder.getTimestamp();
        };
    }

    @Override
    protected Callable<Optional<BrokerMessage>> getUndoAction() {
        return () -> {
            getMatcherData().removeOrder(this.modifiedOrder);
            getMatcherData().addOrder(this.orderBeforeModification);
            this.modifiedOrder = null;
            this.orderBeforeModification = null;
            return Optional.of(this.modificationOrder);
        };
    }
}
