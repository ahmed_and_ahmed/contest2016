package com.gft.digitalbank.exchange.solution.utils;

import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public final class JsonDeserializerFactory {

    private static final JsonDeserializer INSTANCE = new JsonDeserializerImpl();

    public JsonDeserializer getInstance() {
        return INSTANCE;
    }

    private static class JsonDeserializerImpl implements JsonDeserializer {

        private Gson gson;

        public JsonDeserializerImpl(){
            gson = new Gson();
        }

        @Override
        public <T> T fromJson(String json, Type typeOfT) {
            return gson.fromJson(json, typeOfT);
        }
    }
}
