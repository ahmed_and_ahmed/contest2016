package com.gft.digitalbank.exchange.solution.models;


import com.gft.digitalbank.exchange.listener.ProcessingListener;
import com.gft.digitalbank.exchange.solution.utils.DefaultExecutorServiceFactory;
import com.gft.digitalbank.exchange.solution.utils.ExecutorName;
import com.gft.digitalbank.exchange.solution.utils.ExecutorServiceFactory;
import com.gft.digitalbank.exchange.solution.utils.JsonDeserializerFactory;
import com.gft.digitalbank.exchange.solution.utils.WithLogger;

import org.apache.activemq.ActiveMQConnectionFactory;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.jms.ConnectionFactory;
import javax.naming.InitialContext;

public final class Context implements WithLogger {

    private final ExecutorServiceFactory defaultExecutorServiceFactory;
    private final JsonDeserializerFactory jsonDeserializerFactory;
    private ActiveMQConnectionFactory connectionFactory;
    private List<String> destinations;
    private ProcessingListener processingListener;

    public Context(){
        this.defaultExecutorServiceFactory = new DefaultExecutorServiceFactory();
        CompletableFuture.runAsync(() -> {
            DefaultExecutorServiceFactory factory = DefaultExecutorServiceFactory.getInstance();
            factory.getInstance(ExecutorName.Execution);
            factory.getInstance(ExecutorName.MatchingService);
            factory.getInstance(ExecutorName.Matchers);
        });
        this.jsonDeserializerFactory = new JsonDeserializerFactory();
        try {
            this.connectionFactory = (ActiveMQConnectionFactory) (new InitialContext()).lookup("ConnectionFactory");//new ActiveMQConnectionFactory(bindAddress);
        } catch (Exception e) {
            getLogger().error("Error while looking up the ConnectionFactory in the InitialContext", e);
        }
    }

    public void setDestinations(List<String> destinations){
        this.destinations = destinations;
    }

    public void setProcessingListener(ProcessingListener processingListener) {
        this.processingListener = processingListener;
    }

    public List<String> getDestinations() {
        return destinations;
    }

    public ExecutorServiceFactory getExecutorFactory() {
        return defaultExecutorServiceFactory;
    }

    public ProcessingListener getProcessingListener() {
        return processingListener;
    }

    public JsonDeserializerFactory getJsonDeserializerFactory() {
        return jsonDeserializerFactory;
    }

    public ConnectionFactory getJMSConnectionFactory() {
        return connectionFactory;
    }
}
