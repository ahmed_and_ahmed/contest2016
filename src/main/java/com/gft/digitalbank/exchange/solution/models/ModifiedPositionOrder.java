package com.gft.digitalbank.exchange.solution.models;

import com.gft.digitalbank.exchange.model.orders.ModificationOrder;
import com.gft.digitalbank.exchange.model.orders.PositionOrder;


public final class ModifiedPositionOrder extends PositionOrder {

    public ModifiedPositionOrder(PositionOrder positionOrder, ModificationOrder modification) {
        super(modification.getTimestamp(),
                positionOrder.getId(),
                positionOrder.getBroker(),
                positionOrder.getClient(),
                positionOrder.getProduct(),
                positionOrder.getSide(),
                modification.getDetails());
    }
}
