package com.gft.digitalbank.exchange.solution.services.matcher;

import com.gft.digitalbank.exchange.model.Transaction;
import com.gft.digitalbank.exchange.model.orders.BrokerMessage;
import com.gft.digitalbank.exchange.model.orders.CancellationOrder;
import com.gft.digitalbank.exchange.model.orders.ModificationOrder;
import com.gft.digitalbank.exchange.model.orders.PositionOrder;
import com.gft.digitalbank.exchange.solution.models.Context;
import com.gft.digitalbank.exchange.solution.models.Order;
import com.gft.digitalbank.exchange.solution.services.MatcherListener;
import com.gft.digitalbank.exchange.solution.services.matcher.commands.BrokerMessageCommand;
import com.gft.digitalbank.exchange.solution.services.matcher.commands.BrokerMessageCommandFactory;
import com.gft.digitalbank.exchange.solution.services.matcher.commands.Command;
import com.gft.digitalbank.exchange.solution.services.matcher.commands.TransactionCommand;
import com.gft.digitalbank.exchange.solution.utils.ExecutorName;
import com.gft.digitalbank.exchange.solution.utils.WithLogger;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.PriorityBlockingQueue;

public final class Matcher implements WithLogger {

    private final Context context;
    private final CommandRunner commandRunner;
    private final MatcherData matcherData;
    private final MatcherListener listener;
    private final Queue<BrokerMessage> bufferedMessages;
    private volatile boolean isTerminated = false;
    private final Set<String> productOrderUniqueIds = new LinkedHashSet<>();
    private final BrokerMessageCommandFactory brokerMessageCommandFactory;

    private static final int BUFFER_SIZE = 100;

    public Matcher(String product, MatcherListener listener, Context context) {
        this.context = context;
        this.commandRunner = new CommandRunner();
        this.matcherData = new MatcherData(product);
        this.brokerMessageCommandFactory = new BrokerMessageCommandFactory(matcherData);
        this.listener = listener;
        this.bufferedMessages = new PriorityBlockingQueue<>(BUFFER_SIZE, (m1, m2) -> Long.compare(m1.getTimestamp(), m2.getTimestamp()));;
    }

    public OrderBookAndTransactionsProvider getOrderBookAndTransactionsProvider() {
        // This is not thread safe
        return matcherData;
    }

    public void start() {
        CompletableFuture.runAsync(() -> {
            do {
                consumeOrdersQueue();
            } while (!(isTerminated && bufferedMessages.isEmpty()));
            listener.matcherClosed(this);
        }, context.getExecutorFactory().getInstance(ExecutorName.Matchers));
    }

    public String getProduct() {
        return matcherData.getProduct();
    }

    public void addMessage(BrokerMessage message) {
        bufferedMessages.add(message);
    }

    public void terminate() {
        isTerminated = true;
    }

    private boolean orderExists(String brokerName, int orderId) {
        return productOrderUniqueIds.contains(Order.buildUniqueId(brokerName, orderId));
    }

    private void consumeOrdersQueue() {
        BrokerMessage msg = bufferedMessages.poll();
        if (msg != null) {
            if (commandRunner.shouldUndoCommands(msg.getTimestamp())) {
                List<BrokerMessage> messages = commandRunner.undoToTimestampAndReturnOriginalMessages(msg.getTimestamp());
                messages.add(msg);
                bufferedMessages.addAll(messages);
                msg = bufferedMessages.poll();
            }

            if (msg instanceof PositionOrder) {
                productOrderUniqueIds.add(Order.buildUniqueId(msg.getBroker(), msg.getId()));
            } else {
                int orderId;
                if (msg instanceof ModificationOrder) {
                    orderId = ((ModificationOrder) msg).getModifiedOrderId();
                } else if (msg instanceof CancellationOrder) {
                    orderId = ((CancellationOrder) msg).getCancelledOrderId();
                } else {
                    String errorMsg = String.format("Unknown message type: %s", msg.getClass().getSimpleName());
                    getLogger().error(errorMsg);
                    throw new IllegalArgumentException(errorMsg);
                }
                if (!orderExists(msg.getBroker(), orderId)) {
                    return;
                }
            }
            BrokerMessageCommand command = brokerMessageCommandFactory.getCommand(msg);
            commandRunner.doCommand(command);
            matchAllAvailable();
        }
    }


    private void matchAllAvailable() {
        matchOneAndGetCommand().ifPresent((cmd) -> {
            commandRunner.doCommand(cmd);
            matchAllAvailable();
        });
    }

    private Optional<Command> matchOneAndGetCommand() {
        PriorityQueue<Order.BuyOrder> buyWorkBook = matcherData.getBuyWorkBook();
        PriorityQueue<Order.SellOrder> sellWorkBook = matcherData.getSellWorkBook();
        Optional<Transaction> optTransaction;
        final Order.BuyOrder buyOrder = buyWorkBook.peek();
        final Order.SellOrder sellOrder = sellWorkBook.peek();
        if (buyOrder == null || sellOrder == null) return Optional.empty();
        if (buyOrder.getPrice() >= sellOrder.getPrice()) {
            return Optional.of(new TransactionCommand(matcherData, buyOrder, sellOrder, matcherData.nextTransactionId()));
        }
        return Optional.empty();
    }



}
