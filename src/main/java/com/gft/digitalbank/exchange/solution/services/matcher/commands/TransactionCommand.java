package com.gft.digitalbank.exchange.solution.services.matcher.commands;

import com.gft.digitalbank.exchange.model.Transaction;
import com.gft.digitalbank.exchange.model.orders.BrokerMessage;
import com.gft.digitalbank.exchange.model.orders.Side;
import com.gft.digitalbank.exchange.solution.models.Order;
import com.gft.digitalbank.exchange.solution.services.matcher.MatcherData;

import java.util.Optional;
import java.util.PriorityQueue;
import java.util.concurrent.Callable;

public final class TransactionCommand extends AbstractCommand {

    private final Transaction transaction;
    private final Order.BuyOrder buyOrder;
    private final Order.SellOrder sellOrder;
    private CommandStatus status;

    public TransactionCommand(MatcherData matcherData, Order.BuyOrder buyOrder, Order.SellOrder sellOrder, int transactionId) {
        super(matcherData, Long.max(buyOrder.getTimestamp(), sellOrder.getTimestamp()));
        this.buyOrder = buyOrder;
        this.sellOrder = sellOrder;
        int transactionPrice = (buyOrder.getTimestamp() < sellOrder.getTimestamp() ? buyOrder.getPrice() : sellOrder.getPrice());
        int amount = Math.min(buyOrder.getAmount(), sellOrder.getAmount());
        this.transaction = new Transaction(
                transactionId,
                amount,
                transactionPrice,
                getProduct(),
                buyOrder.getBroker(),
                sellOrder.getBroker(),
                buyOrder.getClient(),
                sellOrder.getClient()
        );
    }

    @Override
    public String toString(){
        return String.format("TransactionCommand( buy=%s ,\n sell=%s ,\n transaction=%s )",
                buyOrder.toString(),
                sellOrder.toString(),
                transaction.toString());
    }




    @Override
    protected Callable<Long> getDoAction() {
        return () -> {
            int amount = transaction.getAmount();
            getMatcherData().getTransactions().add(transaction);
            decreaseOrRemoveOrder(buyOrder, amount);
            decreaseOrRemoveOrder(sellOrder, amount);
            return Long.max(buyOrder.getTimestamp(), sellOrder.getTimestamp());
        };
    }

    @Override
    protected Callable<Optional<BrokerMessage>> getUndoAction() {
        return () -> {
            int amount = transaction.getAmount();
            getMatcherData().getTransactions().remove(transaction);

            addToWorkBookOrIncrementAmount(getMatcherData().getBuyWorkBook(), buyOrder, amount);
            addToWorkBookOrIncrementAmount(getMatcherData().getSellWorkBook(), sellOrder, amount);

            return Optional.empty();
        };
    }





    private PriorityQueue<? extends Order> getWorkBookByOrderSide(Side side) {
        return (side.equals(Side.BUY) ? getMatcherData().getBuyWorkBook() : getMatcherData().getSellWorkBook());
    }


    private void decreaseOrRemoveOrder(Order order, int amount) {
        if (order.getAmount() > amount) {
            order.decreaseAndGetAmount(amount);
        } else {
            getWorkBookByOrderSide(order.getSide()).remove(order);
        }
    }

    private <T extends Order>void addToWorkBookOrIncrementAmount(
            PriorityQueue<T> workBook,
            T order,
            int amount
    ){
        if (workBook.contains(order)) {
            order.increaseAndGetAmount(amount);
        } else {
            workBook.add(order);
        }
    }
}
