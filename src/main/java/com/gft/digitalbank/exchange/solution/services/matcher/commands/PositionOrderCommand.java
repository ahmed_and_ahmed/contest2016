package com.gft.digitalbank.exchange.solution.services.matcher.commands;


import com.gft.digitalbank.exchange.model.orders.BrokerMessage;
import com.gft.digitalbank.exchange.model.orders.PositionOrder;
import com.gft.digitalbank.exchange.solution.models.Order;
import com.gft.digitalbank.exchange.solution.services.matcher.MatcherData;

import java.util.Optional;
import java.util.concurrent.Callable;

public final class PositionOrderCommand extends BrokerMessageCommand {

    private final PositionOrder positionOrder;
    private Order addedOrder;   // used for undo

    public PositionOrderCommand(MatcherData matcherData, PositionOrder positionOrder){
        super(matcherData, positionOrder);
        this.positionOrder = positionOrder;
    }

    @Override
    public String toString(){
        return String.format("PositionOrderCommand( %s )", positionOrder.toString());
    }



    @Override
    protected Callable<Long> getDoAction() {
        return () -> {
            addedOrder = Order.createInstance(positionOrder);
            getMatcherData().addOrder(addedOrder);
            return positionOrder.getTimestamp();
        };
    }

    @Override
    protected Callable<Optional<BrokerMessage>> getUndoAction() {
        return () -> {
            getMatcherData().removeOrder(addedOrder);
            this.addedOrder = null;
            return Optional.of(positionOrder);
        };
    }
}
