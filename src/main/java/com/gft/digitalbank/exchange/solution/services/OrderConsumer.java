package com.gft.digitalbank.exchange.solution.services;

import com.gft.digitalbank.exchange.model.orders.BrokerMessage;
import com.gft.digitalbank.exchange.solution.models.Context;
import com.gft.digitalbank.exchange.solution.models.MessageTypeAndClass;
import com.gft.digitalbank.exchange.solution.utils.JsonDeserializer;

import java.util.concurrent.CountDownLatch;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

public final class OrderConsumer implements Consumer {

    private final CountDownLatch countDownLatch;
    private final MessageProcessor messageProcessor;
    private final Context context;
    private final String destinationName;
    private final MessageConsumer consumer;
    private final Session session;
    private final Connection connection;
    private volatile boolean closed = false;

    public OrderConsumer(
            Context context,
             String destinationQueueName,
             CountDownLatch countDownLatch,
             MessageProcessor messageProcessor
    ) {
        this.context = context;
        this.destinationName = destinationQueueName;
        this.countDownLatch = countDownLatch;
        this.messageProcessor = messageProcessor;
        try {
            this.connection = context.getJMSConnectionFactory().createConnection();
            this.connection.setExceptionListener(this);
            this.session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destinationQueue = session.createQueue(this.destinationName);
            this.consumer = session.createConsumer(destinationQueue);
            this.consumer.setMessageListener(this);
        } catch (Exception e) {
            getLogger().error(e.getMessage(), e);
            close();
            throw new RuntimeException(e);
        }
    }

    @Override
    public void startReceivingMessages() {
        try {
            this.connection.start();
        } catch (Exception e) {
            getLogger().error(e.getMessage(), e);
            close();
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        if (closed) return;
        countDownLatch.countDown();
        try {
            consumer.close();
            session.close();
            connection.close();
        } catch (JMSException e) {
            getLogger().error(e.getMessage(), e);
        } finally {
            closed = true;
        }
    }

    @Override
    public void onException(JMSException e) {
        getLogger().error(e.getMessage(), e);
        throw new RuntimeException(e);
    }


    @Override
    public void onMessage(Message message) {
        try {
            JsonDeserializer jsonDeserializer = context.getJsonDeserializerFactory().getInstance();
            MessageTypeAndClass msgType = MessageTypeAndClass.valueOf(message.getStringProperty("messageType"));
            if (msgType.equals(MessageTypeAndClass.SHUTDOWN_NOTIFICATION)) {
                close();
            } else {
                TextMessage textMessage = (TextMessage) message;
                BrokerMessage receivedMessage = jsonDeserializer.fromJson(textMessage.getText(), msgType.getMessageClass());
                messageProcessor.processMessage(receivedMessage);
            }
        } catch (JMSException e) {
            getLogger().error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

}