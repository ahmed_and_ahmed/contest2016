package com.gft.digitalbank.exchange.solution.utils;

public enum ExecutorName {

    Execution(1),
    MatchingService(1),
    Matchers( Integer.max(Runtime.getRuntime().availableProcessors()-2, 1) );

    private final int threadCount;


    public int getThreadCount(){
        return threadCount;
    }

    ExecutorName(int threadCount){
        this.threadCount = threadCount;
    }
}
