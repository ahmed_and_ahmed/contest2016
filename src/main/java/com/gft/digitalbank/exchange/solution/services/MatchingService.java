package com.gft.digitalbank.exchange.solution.services;

import com.gft.digitalbank.exchange.model.OrderBook;
import com.gft.digitalbank.exchange.model.SolutionResult;
import com.gft.digitalbank.exchange.model.Transaction;
import com.gft.digitalbank.exchange.model.orders.BrokerMessage;
import com.gft.digitalbank.exchange.model.orders.CancellationOrder;
import com.gft.digitalbank.exchange.model.orders.ModificationOrder;
import com.gft.digitalbank.exchange.model.orders.PositionOrder;
import com.gft.digitalbank.exchange.solution.models.Context;
import com.gft.digitalbank.exchange.solution.services.matcher.Matcher;
import com.gft.digitalbank.exchange.solution.services.matcher.OrderBookAndTransactionsProvider;
import com.gft.digitalbank.exchange.solution.utils.ExecutorName;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

public final class MatchingService implements MessageProcessor, MatcherListener {

    private final Map<String, Matcher> matchers;
    private final Context context;
    private CountDownLatch matchersCountdownLatch;

    public MatchingService(Context context) {
        this.context = context;
        this.matchers = new ConcurrentHashMap<>();
    }

    @Override
    public void allMessagesProcessed() {
        CompletableFuture.runAsync(() -> {
            matchersCountdownLatch = new CountDownLatch(matchers.size());
            matchers.values().forEach(Matcher::terminate);
            try {
                matchersCountdownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                Set<OrderBook> orderBooks = new HashSet<>();
                Set<Transaction> transactions = new HashSet<>();
                matchers.values().forEach((matcher) -> {
                    OrderBookAndTransactionsProvider orderBookAndTransactionsProvider = matcher.getOrderBookAndTransactionsProvider();
                    orderBookAndTransactionsProvider.getOrderBook().ifPresent(orderBooks::add);
                    transactions.addAll(orderBookAndTransactionsProvider.getTransactions());
                });
                matchers.clear();
                this.context.getProcessingListener().processingDone(SolutionResult.builder().transactions(transactions).orderBooks(orderBooks).build());
            }
        }, context.getExecutorFactory().getInstance(ExecutorName.MatchingService));
    }

    @Override
    public void processMessage(BrokerMessage message) {
        if (message instanceof PositionOrder) {
            getMatcher(((PositionOrder)message).getProduct()).addMessage(message);
        } else if (message instanceof ModificationOrder || message instanceof CancellationOrder) {
            matchers.values().forEach((matcher) -> {
                matcher.addMessage(message);
            });
        }
    }

    @Override
    public void matcherClosed(Matcher matcher) {
        matchersCountdownLatch.countDown();
    }



    private synchronized Matcher getMatcher(String product) {
        return matchers.computeIfAbsent(product, (prod) -> {
            Matcher m = new Matcher(prod, this, context);
            m.start();
            return m;
        });
    }

}
