package com.gft.digitalbank.exchange.solution.services.matcher.commands;

import com.gft.digitalbank.exchange.model.orders.BrokerMessage;
import com.gft.digitalbank.exchange.solution.services.matcher.MatcherData;

public abstract class BrokerMessageCommand extends AbstractCommand {

    protected BrokerMessageCommand(MatcherData matcherData, BrokerMessage message) {
        super(matcherData, message.getTimestamp());
    }

}
