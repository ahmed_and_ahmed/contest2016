package com.gft.digitalbank.exchange.solution.utils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public final class DefaultExecutorServiceFactory implements ExecutorServiceFactory {

    private static final DefaultExecutorServiceFactory INSTANCE = new DefaultExecutorServiceFactory();
    private Map<ExecutorName, ExecutorService> executors = new ConcurrentHashMap<>();

    public static DefaultExecutorServiceFactory getInstance(){
        return INSTANCE;
    }

    @Override
    public ExecutorService getInstance(ExecutorName name) {
        return executors.computeIfAbsent(name, (executorName) -> Executors.newFixedThreadPool(name.getThreadCount(), new ThreadFactory() {
            private AtomicInteger threadAutoId = new AtomicInteger(0);

            @Override
            public Thread newThread(Runnable runnable) {
                return new Thread(runnable, String.format("%s-%d", name, threadAutoId.incrementAndGet()));
            }
        }));
    }
}
