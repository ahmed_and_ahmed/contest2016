package com.gft.digitalbank.exchange.solution.services.matcher.commands;

import com.gft.digitalbank.exchange.model.orders.BrokerMessage;
import com.gft.digitalbank.exchange.model.orders.CancellationOrder;
import com.gft.digitalbank.exchange.solution.models.Order;
import com.gft.digitalbank.exchange.solution.services.matcher.MatcherData;

import java.util.Optional;
import java.util.concurrent.Callable;

public final class CancelOrderCommand extends BrokerMessageCommand {

    private final CancellationOrder cancellationOrder;
    private Order cancelledOrder;

    public CancelOrderCommand(MatcherData matcherData, CancellationOrder cancellationOrder){
        super(matcherData, cancellationOrder);
        this.cancellationOrder = cancellationOrder;
    }

    @Override
    public String toString(){
        return String.format("CancelOrderCommand( %s )", cancellationOrder.toString());
    }



    @Override
    protected Callable<Long> getDoAction() {
        return () -> {
            getMatcherData().findOrder(cancellationOrder.getBroker(), cancellationOrder.getCancelledOrderId())
                    .ifPresent((order) -> {
                        cancelledOrder = order;
                        getMatcherData().removeOrder(order);
                    });
            return cancellationOrder.getTimestamp();
        };
    }

    @Override
    protected Callable<Optional<BrokerMessage>> getUndoAction() {
        return () -> {
            getMatcherData().addOrder(cancelledOrder);
            this.cancelledOrder = null;
            return Optional.of(cancellationOrder);
        };
    }
}
