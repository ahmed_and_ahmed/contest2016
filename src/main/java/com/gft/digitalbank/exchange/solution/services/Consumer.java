package com.gft.digitalbank.exchange.solution.services;

import com.gft.digitalbank.exchange.solution.utils.WithLogger;

import java.util.concurrent.Future;

import javax.jms.ExceptionListener;
import javax.jms.MessageListener;

public interface Consumer extends MessageListener, ExceptionListener, WithLogger {

    void startReceivingMessages();

    void close();
}
